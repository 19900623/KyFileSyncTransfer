﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace KyFileSyncTransfer
{
    public partial class frmTest : Form
    {
        private string getFilesJsonStr = null;
        private HttpContent getFilesContent = null;
        public frmTest()
        {
            InitializeComponent();
        }

        private async void btnTestGetFiles_Click1(object sender, EventArgs e)
        {
            string apiUrl = textBox1.Text.Trim();
            HttpClient client = new HttpClient();
            var result = await client.GetAsync(apiUrl);
            getFilesJsonStr = await result.Content.ReadAsStringAsync();
            var jResult = JsonConvert.DeserializeObject<Dictionary<string, byte[]>>(getFilesJsonStr);
            MessageBox.Show(jResult.Count.ToString());
        }


        private async void btnTestGetFiles_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            string apiUrl = textBox1.Text.Trim();
            HttpClient client = new HttpClient();
            var result = await client.GetAsync(apiUrl);
            //MessageBox.Show(getFilesContent.Headers.ContentLength.ToString());
            //var ms = new MemoryStream();
            //result.Content.CopyToAsync(ms).Wait();
            //getFilesContent = new StreamContent(ms);
            //var sr= result.Content.ReadAsStreamAsync().Result;
            //var filesCont = new MultipartContent();
            //filesCont.Add(new StreamContent(sr));
            //getFilesContent = filesCont;

            var filesCont = new MultipartContent();
            var provider = await result.Content.ReadAsMultipartAsync();
            foreach (var item in provider.Contents)
            {
                string fileName = item.Headers.ContentDisposition.FileName;
                if (!string.IsNullOrEmpty(fileName))
                {
                    filesCont.Add(item);
                    listBox1.Items.Add(fileName);
                }
            }
            getFilesContent = filesCont;

            MessageBox.Show("ok!");
        }

        private async void btnSaveFiles_Click1(object sender, EventArgs e)
        {
            string apiUrl = textBox2.Text.Trim();
            HttpClient client = new HttpClient();
            var result = await client.PostAsync(apiUrl, new StringContent(getFilesJsonStr, Encoding.UTF8, "text/json"));
            MessageBox.Show(result.IsSuccessStatusCode.ToString());
        }

        private async void btnSaveFiles_Click(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            string apiUrl = textBox2.Text.Trim();
            HttpClient client = new HttpClient();
            var result = await client.PostAsync(apiUrl, getFilesContent);
            MessageBox.Show(result.IsSuccessStatusCode.ToString());
            string rsCont = result.Content.ReadAsStringAsync().Result;
            if (!result.IsSuccessStatusCode)
            {
                MessageBox.Show(rsCont);
            }
            else
            {
                var jDic = JsonConvert.DeserializeObject<Dictionary<string, string>>(rsCont);
                foreach (var item in jDic)
                {
                    listBox2.Items.Add(item.Value);
                }
            }
        }

        private async void btnDeleteFiles_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Count <= 0)
            {
                MessageBox.Show("not found need be delete files");
                return;
            }

            string apiUrl = textBox1.Text.Trim();
            HttpClient client = new HttpClient();
            await client.PutAsJsonAsync(apiUrl, listBox1.Items.Cast<string>());
        }

        private async void btnWriteLogs_Click(object sender, EventArgs e)
        {
            if (listBox2.Items.Count <= 0)
            {
                MessageBox.Show("save result is null,please save files at first");
                return;
            }

            string apiUrl = textBox3.Text.Trim();
            HttpClient client = new HttpClient();
            var result = await client.PostAsJsonAsync(apiUrl, listBox2.Items.Cast<string>());
            MessageBox.Show(result.IsSuccessStatusCode.ToString());
        }

        private void frmTest_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dbnull = DBNull.Value;
            string r = dbnull + "";
            MessageBox.Show(r);
        }
    }
}
