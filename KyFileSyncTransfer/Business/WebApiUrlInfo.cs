﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KyFileSyncTransfer.Business
{
    public class WebApiUrlInfo
    {
        public string Url { get; set; }

        public WebApiUrlInfo()
        { }

        public WebApiUrlInfo(string url)
        {
            this.Url = url;
        }

        public override string ToString()
        {
            return this.Url ?? string.Empty;
        }
    }
}
